#include <iostream>
#include <SFML/Graphics.hpp>
#include "compl.h"
#include "mandelbrot.h"
#include <vector>
#include <math.h>
#include <thread>
#include "palette.h"
#include <ctime>
#include <cstdlib>

using namespace std;

/*
TODO:
paletteRepeat
make it FASTER
*/

const double imageHeight = 400;
const double imageWidth = 400;
const int threadNum = 6;
const double paletteRepeat = 1.0;

void DrawAll(sf::RenderWindow& window, vector<sf::RectangleShape*>& pixels) {
    for (int i=0;i<imageWidth;++i) {
        for (int j=0;j<imageHeight;++j) {
            window.draw(*(pixels[i+imageWidth*j]));
        }
    }
}

void Setup(vector<sf::RectangleShape*>& pixels) {
    for (int i=0;i<imageWidth;++i) {
        for (int j=0;j<imageHeight;++j) {
            pixels.push_back(new sf::RectangleShape(sf::Vector2f(1.0f,1.0f)));
        }
    }
    for (int i=0;i<imageWidth;++i) {
        for (int j=0;j<imageHeight;++j) {
            pixels[i+j*(int)imageWidth]->setPosition(sf::Vector2f((float)i,imageHeight-(float)j-1));
        }
    }
}

/*
inline void Generate(GenParams& params) {
    for (float x=0;x<params.mandel.imageWidth;++x) {
        for (float y=0;y<params.mandel.imageHeight;++y) {
            if ((int)(x+y*params.mandel.imageWidth) % params.threads == params.id) {
                Compl c(params.mandel.startX+x*params.mandel.stepX,params.mandel.startY+y*params.mandel.stepY);
                Compl z = c;
                int iter = 0;
                float realsq = z.real*z.real;
                float imagsq = z.imag*z.imag;
                while (z.abssq() < 4.0f && iter < params.mandel.maxIter) {
                    z.real = realsq - imagsq + c.real;
                    z.imag = (z.real+z.imag)*(z.real+z.imag) - realsq - imagsq + c.imag;
                    realsq = z.real*z.real;
                    imagsq = z.imag*z.imag;
                    z = z * z + c;
                    iter++;
                }
                if (((int)floor(x)+(int)floor(y)*(int)params.mandel.imageWidth) < (int)(params.mandel.imageWidth*params.mandel.imageHeight)) {
                    if (iter < params.mandel.maxIter) {
                        float percent = (float)iter/(float)params.mandel.maxIter;
                        params.pixels[(int)floor(x)+(int)floor(y)*(int)params.mandel.imageWidth]->setFillColor(sf::Color((int)(percent*255.0f),(int)(percent*255.0f),(int)(percent*255.0f)));
                    } else {
                        params.pixels[(int)floor(x)+(int)floor(y)*(int)params.mandel.imageWidth]->setFillColor(sf::Color::Black);
                    }
                }
            }
        }
    }
}*/

inline void Generate(int id, int threads, Mandelbrot& mandel, vector<sf::RectangleShape*>& pixels, Palette& palette) {
    for (long double x=0;x<mandel.imageWidth;++x) {
        for (long double y=0;y<mandel.imageHeight;++y) {
            if ((int)(x+y*mandel.imageWidth) % threads == id) {
                Compl c(mandel.startX+x*mandel.stepX,mandel.startY+y*mandel.stepY);
                Compl z = c;
                int iter = 0;
                /*
                float realsq = z.real*z.real;
                float imagsq = z.imag*z.imag;
                */
                while (z.abssq() < 4.0L && iter < mandel.maxIter) {
                    /*
                    z.real = realsq - imagsq + c.real;
                    z.imag = (z.real+z.imag)*(z.real+z.imag) - realsq - imagsq + c.imag;
                    realsq = z.real*z.real;
                    imagsq = z.imag*z.imag;
                    */
                    z = z * z + c;
                    iter++;
                }
                if (iter < mandel.maxIter) {
                    long double percent = (long double)iter/(long double)mandel.maxIter;
                    //int index = (int)(percent*(double)palette.paletteSize) % (int)((double)palette.paletteSize/paletteRepeat);
                    int index = iter % palette.paletteSize;
                    pixels[(int)floor(x)+(int)floor(y)*(int)mandel.imageWidth]->setFillColor(sf::Color(palette.colorPalette[index].r,palette.colorPalette[index].g,palette.colorPalette[index].b));
                } else {
                    pixels[(int)floor(x)+(int)floor(y)*(int)mandel.imageWidth]->setFillColor(sf::Color::Black);
                }
            }
        }
    }
}

void GenerateImage(int id, int threads, int maxIter, sf::Image& image, double width, double height, Mandelbrot& mandel, Palette& palette) {
    long double imageStepX = mandel.sizeX / width;
    long double imageStepY = mandel.sizeY / height;
    for (long double x=0;x<width;++x) {
        for (long double y=0;y<height;++y) {
            if ((int)(x+y*width) % threads == id) {
                Compl c(mandel.startX+x*imageStepX,mandel.startY+y*imageStepY);
                Compl z = c;
                int iter = 0;
                /*
                float realsq = z.real*z.real;
                float imagsq = z.imag*z.imag;
                */
                while (z.abssq() < 4.0 && iter < maxIter) {
                    /*
                    z.real = realsq - imagsq + c.real;
                    z.imag = (z.real+z.imag)*(z.real+z.imag) - realsq - imagsq + c.imag;
                    realsq = z.real*z.real;
                    imagsq = z.imag*z.imag;
                    */
                    z = z * z + c;
                    iter++;
                }
                if (iter < maxIter) {
                    long double percent = (long double)iter/(long double)maxIter;
                    //int index = (int)(percent*(double)palette.paletteSize) % (int)((double)palette.paletteSize/paletteRepeat);
                    int index = iter % palette.paletteSize;
                    image.setPixel((int)x,height-(int)y-1,sf::Color(palette.colorPalette[index].r,palette.colorPalette[index].g,palette.colorPalette[index].b));
                }/* else {
                    image.setPixel((int)x,(int)y,sf::Color::Black);
                }*/
            }
        }
    }
}

sf::RenderWindow window(sf::VideoMode(imageWidth,imageHeight),"My own Mandelbrot-viewer");
sf::RenderWindow setupWindow(sf::VideoMode(imageWidth,imageHeight),"Palette preview");
Mandelbrot mandel(-2.0L,-1.5L,3.0L,3.0L,imageWidth,imageHeight,200);
vector<sf::RectangleShape*> pixels;
bool isChanged = true;
bool generateImage = false;
int generatedImageWidth = 2000;
int generatedImageHeight = 2000;

int main()
{
    Setup(pixels);
    vector<thread*> threads;
    srand(time(0));

    /*
    for (int i=0;i<threadNum;++i) {
        threads.push_back(new thread());
    }
    */

    Palette palette;
    palette.addControlPoint(ControlPoint(Color(230,10,0),30));
    palette.addControlPoint(ControlPoint(Color(20,0,200),120));
    palette.addControlPoint(ControlPoint(Color(0,180,20),170));
    palette.recalculateColors();

    while (setupWindow.isOpen()) {
        sf::Event event;
        while (setupWindow.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                setupWindow.close();
            }
        }
        setupWindow.clear();
        int startX = (imageWidth - palette.paletteSize) / 2;
        for (int i=0;i<palette.paletteSize;++i) {
            sf::RectangleShape rect(sf::Vector2f(1.0f,50.0f));
            rect.setFillColor(sf::Color(palette.colorPalette[i].r,palette.colorPalette[i].g,palette.colorPalette[i].b));
            rect.setPosition(startX+i,imageHeight / 2 - 50);
            setupWindow.draw(rect);
        }
        setupWindow.display();
    }

    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
            if (event.type == sf::Event::KeyPressed) {
                switch (event.key.code) {
                case sf::Keyboard::A:
                    mandel.startX -= mandel.sizeX / 3.0L;
                    isChanged = true;
                    break;
                case sf::Keyboard::D:
                    mandel.startX += mandel.sizeX / 3.0L;
                    isChanged = true;
                    break;
                case sf::Keyboard::W:
                    mandel.startY += mandel.sizeY / 3.0L;
                    isChanged = true;
                    break;
                case sf::Keyboard::S:
                    mandel.startY -= mandel.sizeY / 3.0L;
                    isChanged = true;
                    break;
                case sf::Keyboard::Escape:
                    window.close();
                    break;
                case sf::Keyboard::Q:
                    mandel.startX = mandel.startX - mandel.sizeX / 2.0L;
                    mandel.startY = mandel.startY - mandel.sizeX / 2.0L;
                    mandel.sizeX *= 2.0L;
                    mandel.sizeY *= 2.0L;
                    mandel.reCalcStepIter();
                    isChanged = true;
                    break;
                case sf::Keyboard::E:
                    mandel.sizeX /= 2.0L;
                    mandel.sizeY /= 2.0L;
                    mandel.startX = mandel.startX + mandel.sizeX / 2.0L;
                    mandel.startY = mandel.startY + mandel.sizeX / 2.0L;
                    mandel.reCalcStepIter();
                    isChanged = true;
                    break;
                case sf::Keyboard::I:
                    generateImage = true;
                    break;
                case sf::Keyboard::R:
                    mandel.sizeX = 3.0L;
                    mandel.sizeY = 3.0L;
                    mandel.startX = -2.0L;
                    mandel.startY = -1.5L;
                    mandel.reCalcStepIter();
                    isChanged = true;
                    break;
                }
            }
        }
        window.clear();
        if (isChanged) {
            cout<<"X: "<<mandel.startX<<" Y: "<<mandel.startY<<" step: "<<mandel.stepX<<endl;
            for (int i=0;i<threadNum;++i) {
                threads.push_back(new thread(Generate,i,threadNum,std::ref(mandel),std::ref(pixels),std::ref(palette)));
            }
            for (int i=0;i<threadNum;++i) {
                threads[i]->join();
            }
            isChanged = false;
        }
        if (generateImage) {
            cout<<"Generating image"<<endl;
            sf::Image image;
            image.create(generatedImageWidth,generatedImageHeight);
            for (int i=0;i<threadNum;++i) {
                threads.push_back(new thread(GenerateImage,i,threadNum,mandel.maxIter*10,std::ref(image),generatedImageWidth,generatedImageHeight,std::ref(mandel),std::ref(palette)));
            }
            for (int i=0;i<threadNum;++i) {
                threads[i]->join();
            }
            image.saveToFile("image.bmp");
            generateImage = false;
            cout<<"Generated image"<<endl;
        }
        DrawAll(window,pixels);
        for (int i=0;i<threads.size();++i) {
                delete threads[i];
        }
        threads.clear();
        window.display();
    }
    return 0;
}
