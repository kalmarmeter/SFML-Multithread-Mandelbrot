#ifndef MANDELBROT_H_INCLUDED
#define MANDELBROT_H_INCLUDED

class Mandelbrot {
public:
    long double startX, startY, stepX, stepY, sizeX, sizeY;
    double imageWidth, imageHeight;
    int maxIter;
    Mandelbrot(long double startx, long double starty, long double sizex, long double sizey, double width, double height, int maxit): startX(startx), startY(starty), sizeX(sizex), sizeY(sizey), imageWidth(width), imageHeight(height), maxIter(maxit) {
        stepX = sizeX / imageWidth;
        stepY = sizeY / imageHeight;
    }
    void setImage(double width,double height) {
        imageHeight = height;
        imageWidth = width;
    }
    void reCalcStepIter() {
        stepX = sizeX / imageWidth;
        stepY = sizeY / imageHeight;
        maxIter = 100.0L+1.0L/sizeX * 80.0L;
    }
};

#endif // MANDELBROT_H_INCLUDED
