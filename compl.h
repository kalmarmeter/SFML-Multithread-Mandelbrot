#ifndef COMPL_H_INCLUDED
#define COMPL_H_INCLUDED

#include <cmath>

class Compl {
public:
    long double real,imag;
    Compl(): real(0), imag(0) {}
    Compl(long double r,long double i): real(r), imag(i) {}
    double abssq() const {
        return real*real + imag*imag;
    }
    friend Compl operator+(const Compl& a, const Compl& b) {
        return Compl(a.real+b.real, a.imag+b.imag);
    }
    friend Compl operator*(const Compl& a, const Compl& b) {
        return Compl(a.real*b.real-a.imag*b.imag, a.real*b.imag+a.imag*b.real);
    }
};

#endif // COMPL_H_INCLUDED
