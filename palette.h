#ifndef PALETTE_H_INCLUDED
#define PALETTE_H_INCLUDED

#include <vector>
#include <algorithm>
#include <math.h>

using namespace std;

struct Color {
    int r,g,b;
    Color() {
        r=g=b=0;
    }
    Color(int _r,int _g,int _b): r(_r), g(_g), b(_b) {
        if (r<0 || g<0 || b<0) {
            r=r%255;
            g=g%255;
            b=b%255;
        }
        if (r>255 || g>255 || b>255) {
            r=r%255;
            g=g%255;
            b=b%255;
        }
    }
    void setRGB(int _r,int _g,int _b) {
        r=_r;
        g=_g;
        b=_b;
    }
};

struct ControlPoint {
    Color color;
    int pos;
    ControlPoint(Color c, int p): pos(p) {
        color = c;
    }
    bool operator<(const ControlPoint& other) {
        return (pos<other.pos);
    }
};

int lerp(int beginval, int endval, double prog) {
    return (beginval+(int)((double)(endval-beginval)*prog));
}

class Palette {
public:
    vector<Color> colorPalette;
    vector<ControlPoint> controlPoints;
    int paletteSize;
    Palette() {
        paletteSize=255;
        for (int i=0;i<paletteSize;++i) {
            colorPalette.push_back(Color(0,0,0));
        }
    }
    Palette(int psize): paletteSize(psize) {
        for (int i=0;i<paletteSize;++i) {
            colorPalette.push_back(Color(0,0,0));
        }
    }
    void addControlPoint(ControlPoint cp) {
        controlPoints.push_back(cp);
        //sort(controlPoints.begin(),controlPoints.end());
    }
    void recalculateColors() {
        for (int i=0;i<paletteSize;++i) {
            //Find nearest control point AFTER color
            int j=0;
            while (j<controlPoints.size() && i>controlPoints[j].pos) ++j;
            int prev = (j-1>=0)?j-1:controlPoints.size()-j-1;
            //Calculate the distance and progress between the 2 nearest control points
            int distance = controlPoints[j%controlPoints.size()].pos-controlPoints[(prev)%controlPoints.size()].pos;
            if (distance < 0) {
                distance = paletteSize + distance;
            }
            int distanceFromNearest = i-controlPoints[(prev)%controlPoints.size()].pos;
            if (distanceFromNearest < 0) {
                distanceFromNearest = paletteSize + distanceFromNearest;
            }
            double prog = (double)distanceFromNearest/(double)distance;
            //Linearly interpolate

            /*
            cout<<i<<" prog: "<<prog<<" total distance: "<<distance<<" distancefrom: "<<distanceFromNearest<<endl;
            cout<<controlPoints[j%controlPoints.size()].pos<<" "<<controlPoints[(prev)%controlPoints.size()].pos<<endl;
            */

            int r = lerp(controlPoints[(j-1)%controlPoints.size()].color.r,controlPoints[j%controlPoints.size()].color.r,prog);
            int g = lerp(controlPoints[(j-1)%controlPoints.size()].color.g,controlPoints[j%controlPoints.size()].color.g,prog);
            int b = lerp(controlPoints[(j-1)%controlPoints.size()].color.b,controlPoints[j%controlPoints.size()].color.b,prog);
            colorPalette[i] = Color(r,g,b);
        }
    }
};

#endif // PALETTE_H_INCLUDED
